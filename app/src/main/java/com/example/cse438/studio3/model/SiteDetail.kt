package com.example.cse438.studio3.model

import java.io.Serializable

class SiteDetail(): Serializable {
    private var url: String = ""
    private var name: String = ""
    private var sku: String = ""
    private var recentoffers_count: Int = 0
    private var latestoffers: ArrayList<Offer> = ArrayList()

    constructor(
        url: String,
        name: String,
        sku: String,
        recentoffers_count: Int,
        latestoffers: ArrayList<Offer>
    ): this() {
        this.url = url
        this.name = name
        this.sku = sku
        this.recentoffers_count = recentoffers_count
        this.latestoffers = latestoffers
    }

    fun getUrl(): String {
        return this.url
    }

    fun getName(): String {
        return this.name
    }

    fun getSKU(): String {
        return this.sku
    }

    fun getRecentOffersCount(): Int {
        return this.recentoffers_count
    }

    fun getLatestOffers(): ArrayList<Offer> {
        return this.latestoffers
    }
}